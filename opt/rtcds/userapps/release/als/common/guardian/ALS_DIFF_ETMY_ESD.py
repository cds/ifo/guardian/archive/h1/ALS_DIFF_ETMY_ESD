# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# ALS_DIFF.py
#
# $Id$
# $HeadURL$

import json
import os
import numpy as np
import time
import sys

import cdsutils as cdu
import ISC_library
from ISC_library import darmcarm_outrix, intrix, intrix_OMCAS45
from ezca.ligofilter import LIGOFilter
import ALS_GEN_STATES
from guardian import GuardState, GuardStateDecorator

alsDiffParamsPath = '/opt/rtcds/userapps/trunk/als/common/guardian/alsDiffParams.dat'
with open(alsDiffParamsPath, 'r') as alsDiffParamFile:
    alsDiffParamDict = json.load(alsDiffParamFile)

sys.path.append('/opt/rtcds/userapps/release/isc/h1/guardian/ISC_library')

global counter

import ISC_library
#FIXME importing ISC_library
def MC_locked():
    return ezca.read('GRD-IMC_LOCK_STATE', as_string=True) == 'LOCKED'
##################################################
# is_fault()
# checks various fault states and displays an error
# message in the guardian medm screen.
##################################################
def is_fault():
    message = []
    flag = False
    lockedStates = ['LOCKED_NO_SLOW', 'LOCKED_W_SLOW_FEEDBACK', 'LOCKED_NO_SLOW_NO_WFS', 'ENGAGE_WFS', 'LOCKED_SLOW_NO_WFS', 'LOCKED_RED', 'LOCKED_SLOW_W_ETM_WFS', 'NO_SLOW_W_ETM_WFS']
    if ezca.read('GRD-ALS_XARM_STATE_S', as_string=True) not in lockedStates:
        flag = True
        message.append('XARM')
        log('XARM')
    if ezca.read('GRD-ALS_YARM_STATE_S', as_string=True) not in lockedStates:
        flag = True
        message.append('YARM')
        log('YARM')
    if ezca.read('GRD-SUS_ETMX_STATE_S', as_string=True) == 'TRIPPED':
        flag = True
        message.append('ETMX tripped')
    if ezca.read('GRD-SUS_ETMY_STATE_S', as_string=True) == 'TRIPPED':
        flag = True
        message.append('ETMY tripped')
    '''if ezca['ALS-C_DIFF_A_DEMOD_RFMON'] < -15:
        flag = True
        message.append('DIFF beatnote')
        log('beatnote')'''
    if flag:
        notify(', '.join(message))
    return flag

##################################################
# fault_checker
# brings the guardian to DOWN state when is_fault
# is True
##################################################
class fault_checker(GuardStateDecorator):
    def pre_exec(self):
        if is_fault():
            return 'LOCKLOSS'

##################################################
# STATES
##################################################
nominal = 'SHUTTERED'
request = 'PREPARE'

####################
class INIT(GuardState):
    request = False

    def main(self):
        return 'PREPARE'

class LOCKLOSS(GuardState):
    request = False
    def run(self):
        return True

class DOWN(GuardState):
    index = 100
    goto = True
    def main(self):
        self.first_run = 1;
        ezca['LSC-DARM1_GAIN'] = 0.0
        ezca['LSC-DARM2_GAIN'] = 0.0
        ezca['LSC-DARM1_RSET'] = 2
        ezca['LSC-DARM2_RSET'] = 2
        ezca['SUS-ETMY_L1_LOCK_L_TRAMP'] = 10.0
        ezca['SUS-ETMX_L1_LOCK_L_TRAMP'] = 10.0
        ezca['SUS-ETMY_L1_LOCK_L_GAIN'] = 0.0
        #ezca['SUS-ETMX_L1_LOCK_L_GAIN'] = 0.0
        ezca['SUS-ETMY_BIO_L1_STATEREQ'] = 1
        ezca['SUS-ETMX_M0_TEST_L_OFFSET'] = 0
        ezca.switch('SUS-ETMX_L1_LOCK_L','FM2','FM1', 'FM9','OFF')

        # Turn off the high-bandwidth DIFF configuration
        # used during CARM/DARM transitioning.
        ezca.get_LIGOFilter('LSC-DARM1').only_on('INPUT', 'OUTPUT','DECIMATION', 'FM7')
        ezca.get_LIGOFilter('LSC-DARM2').only_on('INPUT', 'FM3','FM4','FM6', 'FM7', 'FM9', 'FM10', 'OUTPUT', 'DECIMATION')
    
        # set back to the nominal VCO offset.
        ezca['ALS-C_DIFF_VCO_TUNEOFS'] = -1.9
        self.timer['ramp'] = 5

    def run(self):
        if self.timer['ramp'] and ezca['SUS-ETMX_L1_LOCK_L_OUTPUT'] > 10000:
            ezca['SUS-ETMX_L3_LOCK_L_RSET'] = 2.0
            ezca['SUS-ETMX_L1_LOCK_L_RSET'] = 2.0
            ezca['SUS-ETMX_L1_DRIVEALIGN_L2P_RSET'] = 2.0
            log('clearing ETMX history')
        if self.timer['ramp'] and ezca['SUS-ETMY_L1_LOCK_L_OUTPUT'] > 10000:
            ezca['SUS-ETMY_L3_LOCK_L_RSET'] = 2.0
            ezca['SUS-ETMY_L1_LOCK_L_RSET'] = 2.0
            ezca['SUS-ETMY_L1_DRIVEALIGN_L2P_RSET'] = 2.0
            log('clearing ETMY history')
        if self.timer['ramp'] and self.first_run:
            ezca['SUS-ETMY_L1_LOCK_L_RSET'] = 2.0
            ezca['SUS-ETMX_L1_LOCK_L_RSET'] = 2.0
            ezca['SUS-ETMX_L1_DRIVEALIGN_L2P_RSET'] = 2.0
            ezca['SUS-ETMY_L1_DRIVEALIGN_L2P_RSET'] = 2.0
            time.sleep(0.1)
            self.first_run = 0
	    if self.timer['ramp'] and not self.first_run:  
	        return True


class PREPARE(GuardState):
    index = 1
    @fault_checker
    def main(self):
        # prepare DIFF PLL
        log('Setting DIFF PLL parameters')
        ezca['ALS-C_DIFF_PLL_INEN'] = 1
        ezca['ALS-C_DIFF_PLL_POL'] = 0
        ezca['ALS-C_DIFF_PLL_GAIN'] = 26
        ezca['ALS-C_DIFF_PLL_COMP1'] = 1
        ezca['ALS-C_DIFF_PLL_COMP2'] = 1
        ezca.switch('ALS-C_DIFF_PLL_CTRL', 'FM3', 'FM4', 'FM5', 'FM6', 'ON')
        ezca.switch('ALS-C_DIFF_PLL_CTRL', 'OFFSET', 'OFF')
        #ezca.switch('LSC-DARM2', 'FM6', 'FM7', 'FM9', 'FM10', 'INPUT', 'OUTPUT', 'ON')

        # prepare LSC
        log('Preparing the LSC')
        ezca['LSC-DARM2_GAIN'] = 1
        ezca['LSC-DARM1_TRAMP'] = 10
        self.timer['wait'] = 0.5
        ezca['LSC-PD_DOF_MTRX_TRAMP'] = 0.0
        ezca['LSC-ARM_INPUT_MTRX_TRAMP'] = 0.0
        # clear output matrix
        for jj in range(1, 12):
            ezca['LSC-OUTPUT_MTRX_1_%d'%jj] = 0 # clear ETMX row
            ezca['LSC-OUTPUT_MTRX_2_%d'%jj] = 0 # claer ETMY row
		for jj in range(1,3):
			ezca['LSC-ARM_OUTPUT_MTRX_1_%d'%jj] = 0 # clear ETMX
			ezca['LSC-ARM_OUTPUT_MTRX_2_%d'%jj] = 0 # clear ETMY
		# set output matrix
		ISC_library.darmcarm_outrix['ETMX', 'DARM'] = 1
	    ISC_library.darmcarm_outrix['ETMY', 'DARM'] = -1

        #set up input matrix
		ISC_library.intrix.zero(row='DARM') # clear DARM
		ISC_library.intrix_OMCAS45['DARM', 'ALS_DIFF'] = 1.0
		ISC_library.intrix_OMCAS45.load()
		ISC_library.intrix.load()
        matrixGood = ISC_library.loadRampMatrix('LSC-ARM_INPUT_MTRX', [(1, 4)], 3)

        # prepare ETMX suspension
        log('Preparing ETMX suspension')
		ezca['SUS-ETMX_L3_ISCINF_L_GAIN']=1
        ezca.switch('SUS-ETMX_L3_LOCK_L', 'FM2', 'FM6', 'ON')
        ezca['SUS-ETMX_L3_LOCK_L_GAIN'] = 1.0
        ezca['SUS-ETMX_L2_LOCK_L_GAIN'] = 1
        ezca['SUS-ETMX_L1_LOCK_L_TRAMP'] = 2.0
        ezca.get_LIGOFilter('SUS-ETMX_L1_LOCK_L').only_on('INPUT', 'FM8', 'FM5', 'OUTPUT', 'DECIMATION') #FM8 replaces FM3
        ezca['SUS-ETMX_L1_LOCK_L_GAIN'] = 0.4 # 0.4 * 0.7 
        ezca.switch('SUS-ETMX_M0_LOCK_L', 'INPUT', 'OFF')
        #make sure it doesn't go to the ESD
        ezca['SUS-ETMX_L3_DRIVEALIGN_L2L_GAIN']=0

        #prepare ETMY sus (send signal only to L3)
		ezca['SUS-ETMY_L3_ISCINF_L_GAIN']=1
        ezca['SUS-ETMY_L3_LOCK_L_GAIN'] = 1.25
        ezca.get_LIGOFilter('SUS-ETMY_L3_LOCK_L').only_on('INPUT', 'FM2', 'FM6', 'OUTPUT','DECIMATION')
        if ezca['SUS-ETMY_L3_LOCK_INBIAS'] < 0:
            ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN']=-1  #is this the right sign?
        else:
            ezca['SUS-ETMY_L3_DRIVEALIGN_L2L_GAIN']=1  #is this the right sign?

        #make sure signal doesn't go above L3 for ETMY
        ezca['SUS-ETMY_L2_LOCK_L_GAIN'] = 0
        ezca['SUS-ETMY_L1_LOCK_L_TRAMP'] = 2.0
        ezca['SUS-ETMY_L1_LOCK_L_GAIN'] = 0.2 # No longer = 0.670 * 0.7
        ezca.switch('SUS-ETMY_M0_LOCK_L', 'INPUT', 'OFF')



    @fault_checker
    def run(self):
        if not ISC_library.input_matrix_OK():
            notify('check LSC input matrix')
        if abs(ezca['ALS-C_DIFF_PLL_CTRLMON']) >= 8:
            # turn on a very low DARM gain to pull the PLL control away from the
            # rail
            #ezca['LSC-DARM_GAIN'] = 0.1
            dummy=1
        if abs(ezca['ALS-C_DIFF_PLL_CTRLMON']) >= 5:
            self.timer['wait'] = 0.5
            notify('WAITING for DIFF PLL to come in range')
        else:
            if self.timer['wait']:
                notify('ALS DIFF okay')
                return True


class LOCK(GuardState):
    index = 2
    request = False

    @fault_checker
    def main(self):
        ezca['LSC-DARM1_GAIN'] = 40
        self.timer['ramp_gain'] = ezca['LSC-DARM1_TRAMP']
        self.timer['boost'] = 2*ezca['LSC-DARM1_TRAMP']
        self.timer['boost2'] = 30
        self.timer['boost3'] = 35

    @fault_checker
    def run(self):
        if self.timer['ramp_gain']:
            log('Ramping DARM gain')
            ezca['LSC-DARM1_GAIN'] = 400 # from 400 to 600, 2017.June.1, KI # was 600 before change in FM2
        if self.timer['boost']:
            log('Engaging boosts')
            ezca.switch('SUS-ETMX_L1_LOCK_L', 'FM1', 'FM2', 'ON') #FM 9 was FM2
        if self.timer['boost2']:
            log('blah')
            ezca.switch('LSC-DARM2', 'FM5', 'ON')
        if self.timer['boost3']:
            log('done! locked!')
            return True


class LOCKED(GuardState):
    index = 3
    @fault_checker
    def run(self):
        return True

#####################################################################
# FIND_IR
#####################################################################

# Possible offsets that can let you find a Y arm resonance
darmOffsetList = alsDiffParamDict['diffOffsets']

class FIND_IR(GuardState):
    index = 10
    request = True

    @fault_checker
    def main(self):
        self.waitTime = 15
        ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = self.waitTime # to make the offset ramp slow
        ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = 0
        ezca.switch('ALS-C_DIFF_PLL_CTRL', 'OFFSET', 'ON')
        self.timer['wait1'] = self.waitTime
        self.counter = 0

    @fault_checker
    def run(self):
        if self.timer['wait1'] and ezca['LSC-TR_Y_NORM_OUTPUT'] > 0.1: # tries to find a high build up
            ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = 2
            return True
        if self.timer['wait1'] and self.counter == len(darmOffsetList):
            ezca['ALS-C_DIFF_PLL_CTRL_TRAMP'] = 2
            return 'NO_IR_FOUND'
        if self.timer['wait1']:
            ezca['ALS-C_DIFF_PLL_CTRL_OFFSET'] = darmOffsetList[self.counter]
            self.timer['wait1'] = self.waitTime + 5
            self.counter +=1




#COARSE_TUNE_IR = gen_TUNE_IR(300, 30, 5, 6, 'LSC-Y_TR_A_LF_OUTPUT','ALS-C_DIFF_PLL_CTRL')
FINE_TUNE_IR = ALS_GEN_STATES.gen_TUNE_IR_BETTER(30, 3, 2, 0.55, 'LSC-TR_Y_NORM_INMON',
                           'ALS-C_DIFF_PLL_CTRL_OFFSET', 2, 2)
#FINE_TUNE_DITHER = gen_findIrDither('LSC-Y_TR_A_LF_OUTPUT', 120,
#    'SUS-ETMX_L4_TEST_L_EXC', 2.9, 1e4, 'ALS-C_DIFF_PLL_CTRL_OFFSET', 100, 90)
#class  FINE_TUNE_IR(GuardState):
#    @fault_checker
#    def main(self):
#        coarseSweep(.0025, .005, 3)
#        self.timer['wait4'] = 5
#        return True

#####################################################################
# IR_FOUND
#####################################################################
class SAVE_OFFSET(GuardState):
    index = 25
    request = False
    @fault_checker
    def main(self):
        self.NewOffset = ezca['ALS-C_DIFF_PLL_CTRL_OFFSET']
        self.Tolerance = 2000
        self.savedOffsetList = alsDiffParamDict['diffOffsets']
        log(darmOffsetList)
        self.writer = 0
        #write offset to alsparams here
        if ezca['LSC-TR_Y_NORM_INMON'] > 0.7 and ezca['LSC-TR_X_NORM_INMON'] > 0.6:
            for ii in range(1):
                log('checking against saved offset {} whose value is {}'.format(ii, self.savedOffsetList[ii]))
                if abs(self.NewOffset - self.savedOffsetList[ii]) < self.Tolerance and self.writer < 1:
                    log('Replacing saved offset of {} with new offset of {}. '
                        'Trans power is {:.0f}'.format(self.savedOffsetList[ii],
                                                       self.NewOffset,
                                                       ezca['LSC-Y_TR_A_LF_OUTPUT']))
                    darmOffsetList[ii] = ezca['ALS-C_DIFF_PLL_CTRL_OFFSET']
                    # Write the new parameters to the file
                    alsDiffParamDict['diffOffsets'] = darmOffsetList
                    with open(alsDiffParamsPath, 'w+') as alsDiffParamFile:
                        json.dump(alsDiffParamDict, alsDiffParamFile)
                    self.writer = 1
                else:
                    log('not saving offset here, too different from existing offsets')
                if ii == 1:
                    notify('Found offset is different from saved offsets '
                           'by more than {}'.format(self.Tolerance))
        elif ezca['LSC-TR_Y_NORM_INMON'] > 0.7:
            log('not saving offset since IR not high enough in Y arm')
        else:
            log('not saving offset since IR not high enough in X arm')
            #notify('IR not really found?')


class IR_FOUND(GuardState):
    index = 26
    @fault_checker
    def run(self):
            return True


class SHUTTERED(GuardState):
    index = 50
    goto = True

#####################################################################
# NO_IR_FOUND
#####################################################################
class NO_IR_FOUND(GuardState):
    index = 30
    request = False
    redirect = False

    @fault_checker
    def run(self):
        if ezca['LSC-TR_Y_NORM_OUTPUT'] > 0.4:
            return True
        else :
            notify('Sorry!!! Search for IR Resonance by hand!')


##################################################

edges = [
    ('DOWN', 'PREPARE'),
    ('PREPARE', 'LOCK'),
    ('LOCK', 'LOCKED'),
    ('LOCKED', 'FIND_IR'),
    ('FIND_IR', 'FINE_TUNE_IR'),
    ('FINE_TUNE_IR', 'SAVE_OFFSET'), 
    ('SAVE_OFFSET', 'IR_FOUND'),   
    ('IR_FOUND', 'LOCKED'),
    #('FIND_IR', 'COARSE_TUNE_IR'),
    #('COARSE_TUNE_IR', 'FINE_TUNE_IR'),
    #('COARSE_TUNE_IR', 'IR_FOUND'),
    #('FINE_TUNE_IR', 'IR_FOUND'),
    ('NO_IR_FOUND', 'SAVE_OFFSET'),
    ('SAVE_OFFSET', 'IR_FOUND'),
    ('IR_FOUND', 'SHUTTERED'),
    ('NO_IR_FOUND', 'FIND_IR'),
    ]

##################################################
# SVN $Id$
# $HeadURL$
##################################################
