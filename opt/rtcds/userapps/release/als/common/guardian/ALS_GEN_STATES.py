# -*- mode: python; tab-width: 4; indent-tabs-mode: nil -*-
#
# $Id: ISC_GEN_STATES.py 9019 2014-10-31 00:57:27Z sheila.dwyer@LIGO.ORG $
# $HeadURL: https://redoubt.ligo-wa.caltech.edu/svn/cds_user_apps/trunk/isc/h1/guardian/ISC_GEN_STATES.py $

import math
import time

import cdsutils as cdu
import ezca as ez

from guardian import GuardState, GuardStateDecorator

"""
corMax = 20
corSmall = 1
"""

def is_fault():
    message = []
    flag = False
    if ezca['GRD-IMC_LOCK_STATE_N'] < 50:
        flag = True
        message.append('IMC')
    # extract bit 7 from the ODC bit word for the PSL status, this is for ISS saturations
    #if not bin(int(ezca['PSL-ODC_CHANNEL_LATCH'])&128)[-8]:  
    #    flag=True
    #    message.append('ISS Saturation')
    if flag:
        notify(', '.join(message))
    return flag


class fault_checker(GuardStateDecorator):
    def pre_exec(self):
        if is_fault():
            return 'LOCKLOSS'


"""
def gen_findIrDither(powerChan, ditherTimeout, excChan, excFreq, excAmp, ctrlChan,
                     gain, phase):

    # setup
    con = ctrlChan
    mon = excChan + 'MON'

    # start dithering
    excProc = sd.startSineExc(excChan, excFreq, excAmp, ditherTimeout + 10)
    sleep(1)
    print('Dither ON, starting servo')

    # start main loop
    startTime = time()
    nLowCoh = 0
    conList = [ezca[con]]
    while (time() - startTime < ditherTimeout and nLowCoh < 3):
        # Demodulate
        err, coh, mag, phi, sig, pdPower = sd.demod(powerChan, 
            mon, phase, excFreq, duration = 4)

        # correction signals
        powerNorm = max(0.5, pdPower)
        cor = limit(gain * err / powerNorm, -corMax, corMax)

        # compute new position
        conList.append(ezca[con] + cor)

        # move to new position
        ezca[con] = conList[-1]

        # report
        if len(conList) > 3:
          delta = conList[-1] - conList[-3]
        else:
          delta = conList[-1] - conList[0]

        if len(conList) > 3 and np.abs(delta) < corSmall:
            nLowCoh += 1
        else:
            nLowCoh = 0

        # report
        mag = gain * mag / powerNorm
        print('Dither: {:.2f} ({:.3f} at {:.1f}dg) dp {:.2f}, nLow {} power {:.3f}'.format(corPit, mag, phi, delta, nLowCoh, pdPower))

        # Sleep 
        sleep(1)
    
    # Stop excitations
    sd.stopSineExc(excChan, excProc)

    return ezca[Con]
"""


def gen_TUNE_IR_BETTER(SweepWidth, StepSize, pause, Thresh, TransChan, SweepChan, SleepTime, dof):
    #dof should be 1 for comm, 2 for diff
    class TUNE_IR_BETTER(GuardState):
        request = False
        @fault_checker
        def main(self):
            self.SweepWidth = SweepWidth  # width of the sweep is +/- this number
            self.StepSize = StepSize
            self.SleepTime = SleepTime
            self.pause = pause # time to pause for each step (seconds)
            self.threshHigh = Thresh   # threshold on transmitted power to jump to next state
            self.threshLow = 0.02
            self.NumSteps = 2*math.floor(self.SweepWidth/self.StepSize) # total number of steps
            self.counter = 0
            self.StartPoint = ezca[SweepChan]
            self.MaxTrans = ezca[TransChan]
            self.transValueOld = ezca[TransChan]
            self.transValueNew = ezca[TransChan]
            self.direction = 1 # positive or negative
            self.MaxTransOffset = ezca[SweepChan]
            self.timer['pause1'] = 5
            self.timer['pause'] = self.pause
            #check if you don't need a fine tune IR                
            self.skip = False
            if ezca[TransChan] >= self.threshHigh and ezca['ALS-C_TRX_A_LF_OUTMON'] > 0.95:
                log(TransChan+' is already above threshold')
                self.skip = True

        @fault_checker
        def run(self): 
            # check gren arm build ups before finding IR
            if ezca['ALS-C_TRX_A_LF_OUTMON'] < 0.85:
                notify('X arm alignment is bad, not searching')
                time.sleep(1)
            # also check Y arm if serarching for diff offset
            elif dof == 2 and ezca['ALS-C_TRY_A_LF_OUTMON'] < 0.85:
                notify('Y arm alignment is bad, not searching')
                time.sleep(1)
            else:            
                if self.skip == True:
                    return True
                self.transValueNew = cdu.avg(1, TransChan)
                log('Transmitted value is {}'.format(self.transValueNew))
                if self.transValueNew < self.threshLow:
                    log('Transmitted power too low for FINE_TUNE_IR.')
                    return 'NO_IR_FOUND'
                elif self.transValueNew >= self.threshHigh:
                    log('Transmitted power sufficient.')
                    return True
                elif self.counter < self.NumSteps:
                    if self.transValueNew < self.transValueOld:
                        self.direction *= -1
                    ezca[SweepChan] += self.StepSize * self.direction
                    self.transValueOld = self.transValueNew
                    time.sleep(self.SleepTime)
                else:
                    log('Fine tuning failed.')
                    return 'NO_IR_FOUND'
    return TUNE_IR_BETTER

