# 2000 is for 1W input.  It's scaled where it's used in the code
#
# $Id$
# $HeadURL$

import numpy

###########################
## general stuff
###########################
bs_oscillation_thresh = 300 # urads

power_setpoint = 30 # W
#power_setpoint = 35 # W # 47W request gives 50W actual, 10 Oct 2016

use_dc_readout = 1  # Transition DARM to DC readout, or stay on RF?  1=DC, 0=RF

use_popx_wfs = 1 # use the in-air POP X WFS ?

omc_dcpd_sum_target = 20 # in mA
omc_readout_pref = 1.3689         # reference value it had during calibration measurements on 2015/08/30 12:08:43 UTC
omc_readout_err_gain = -8.7614e-7 # reference value it had during calibration measurements on 2015/08/30 12:08:43 UTC
omc_readout_x0 = 15.233           # reference value it had during calibration measurements on 2015/08/30 12:08:43 UTC
omc_sign = 1 # Which side of the DARM fringe?

invac_setpoint = 0 # dB
carm_sensor = 'vac' # must be 'air' or 'vac'

mich_acquire_FMs = []

prcl_acquire_FMs = ['FM4', 'FM9', 'FM10']
prm_m2_FMs = ['FM1', 'FM3', 'FM4', 'FM6', 'FM10']
prm_M2_cross_over =1# reduced because this was sometimes unstable SED 4/23/2015 4.5  # in Hz
prm_M1_cross_over = -0.02
srcl_acquire_FMs = ['FM9', 'FM10']
srm_m1_FMs = ['FM2', 'FM4', 'FM6', 'FM8','FM10']
srm_M1_cross_over = -0.02
#srm_M2_cross_over_start = 1.0
#srm_M2_cross_over = 4.5  # in Hz

###########################
## DRMI
###########################

drmi_locked_threshold_pop18I = 45 #2000

drmi_mich_gain_no_arms = 1
drmi_prcl_gain_no_arms = 15
drmi_srcl_gain_no_arms = -33

drmi_mich_gain_als_lockacq = 2.8#1.4# 4.0
drmi_prcl_gain_als_lockacq = 16#15
drmi_srcl_gain_als_lockacq = -30#-45

drmi_mich_gain_als = 1.4#2.9#4.0  # was 3
drmi_prcl_gain_als = 8 # was 11.0 until Feb 5 2015
drmi_srcl_gain_als = -45

mich_input_mtrx = 4
prcl_input_mtrx = 3.5
srcl_input_mtrx = 4

drmi_modehop_offset = -800

# FM triggers
drmi_mich_fm_trig_index = []#used to have 3
drmi_prcl_fm_trig_index = [2] #used to have 3
drmi_srcl_fm_trig_index = [1] #used to have 4

drmi_srcl_trig_upper_threshold_no_arms = 10#1.1 #POPAIR_A_RF90
drmi_srcl_trig_lower_threshold_no_arms = 5#0.7 #POPAIR_A_RF90
drmi_srcl_trig_upper_threshold_als = 20 #2.2 #POPAIR_A_RF90
drmi_srcl_trig_lower_threshold_als = 10 #1.4 #POPAIR_A_RF90
drmi_mich_trig_upper_threshold_als = 20 # was ten before 10 #POPAIR_A_RF18
drmi_mich_trig_lower_threshold_als = 10 #3 #POPAIR_A_RF18
drmi_mich_trig_upper_threshold_no_arms = 10#5 #POPAIR_A_RF18
drmi_mich_trig_lower_threshold_no_arms = 5#2 #POPAIR_A_RF18
drmi_prcl_trig_upper_threshold = -100.0 #POPAIR_A_RF18
drmi_prcl_trig_lower_threshold = -100.0 #POPAIR_A_RF18
drmi_srcl_fm_trig_upper_threshold_no_arms = 5.5
drmi_srcl_fm_trig_lower_threshold_no_arms = 1.0
drmi_srcl_fm_trig_upper_threshold_als = 5.5
drmi_srcl_fm_trig_lower_threshold_als = 1.0
drmi_prcl_fm_trig_upper_threshold = 3.5
drmi_prcl_fm_trig_lower_threshold = 1.0
drmi_mich_fm_trig_upper_threshold = 25.0 # 4.5
drmi_mich_fm_trig_lower_threshold = 3.0 # 1.0

# ASC
#drmi_asc_engage_threshold = 300 # ASAIR_RF90
#drmi_asc_caution_threshold = 450 # ASAIR_RF90
drmi_asc_engage_threshold = 150 # ASAIR_A_LF
drmi_asc_caution_threshold = 220 # ASAIR_A_LF
mich_asc_P_gain = 0.7
mich_asc_Y_gain =0.7
DC3_P_gain = 0.17
DC3_Y_gain = 0.13
DC4_P_gain = 0.17
DC4_Y_gain = 0.17
###########################
## DRMI 3F
###########################

# KIed
#refl_27_I_gain =  -4.0
#refl_135_I_gain = 1.7
#refl_135_Q_gain = 1.25#2.5

refl_27_I_gain =  -2.0
refl_135_I_gain = 1.7
refl_135_Q_gain = 1.25#2.5


###########################
## DRMI POPAIR
###########################

prclPopair9I = 0.12
srclPopair45I = 0.26
michPopair45Q = 0.26
srclPopair9I = 0.017

###########################
## DRMI POP
###########################

prclPop9I = 0.035
srclPop45I = 0.08
michPop45Q = 0.08
srclPop9I = -0.0151
srclPop9I_25w = -0.03195 # tuned at 30W on 2017/6 Sheila Dwyer
prclPOP9I_25w  = 0.0091 # 26% on POP9, 2016/11/07, Stefan Ballmer
prclREFL45I_25w = -849  # 74% on REFL45, 2016/11/07, Stefan Ballmer


###########################
## prmi sideband
###########################

# for REFL_RF45_I
prmisb_prcl_gain = 11 # 22 #5.25 #5 #when build is 160 in RF18, gain should be more like 2.5

# for REFL_RF9_I
#prmisb_prcl_gain = 2.1

prmisb_prcl_trig_upper_threshold = 3
prmisb_prcl_trig_lower_threshold = 1

prmisb_prcl_trig_fm_upper_threshold = 3.5
prmisb_prcl_trig_fm_lower_threshold = 1.0

prmisb_mich_gain = 15 #27.5 #12.4   # was 4 before RF amp was removed from 45

prmisb_mich_trig_upper_threshold = 10.0
prmisb_mich_trig_lower_threshold =  5

prmisb_mich_trig_fm_upper_threshold = 4.5
prmisb_mich_trig_fm_lower_threshold = 1.0

prmisb_locked_threshold = 25  # SPOP_18

prmi_mich_gain_als = 2.8 #was 1.4
prmi_prcl_gain_als = 16   # was 8

###########################
## prmi car
###########################
prmicar_prcl_gain = -40
prmicar_mich_gain = 30

prmicar_prcl_trig_upper_threshold = 10
prmicar_prcl_trig_lower_threshold = 2

prmicar_prcl_trig_fm_upper_threshold = 15
prmicar_prcl_trig_fm_lower_threshold = 2

#prmicar_mich_trig_upper_threshold = 4.0
#prmicar_mich_trig_lower_threshold = 1.0
prmicar_mich_trig_upper_threshold = 60
prmicar_mich_trig_lower_threshold = 30

#prmicar_mich_trig_fm_upper_threshold = 4.5
#prmicar_mich_trig_fm_lower_threshold = 1.0
prmicar_mich_trig_fm_upper_threshold = 15
prmicar_mich_trig_fm_lower_threshold = 2

prmicar_locked_threshold = 2000 # POP DC

prmi_car_asc_gains = {
    'ASC-MICH_P_GAIN':   -0.8,
    'ASC-MICH_Y_GAIN':   -0.5,
    'ASC-PRC1_P_GAIN':   10.0,
    'ASC-PRC1_Y_GAIN':  -10.0,
    'ASC-PRC2_P_GAIN':   80.0,
    'ASC-PRC2_Y_GAIN': -300.0}

prmi_car_asc_outputs = {
    'ASC-OUTMATRIX_P_1_3': 1,
    'ASC-OUTMATRIX_Y_1_3': 1,
    'ASC-OUTMATRIX_P_3_4': 1,
    'ASC-OUTMATRIX_Y_3_4': 1,
    'ASC-OUTMATRIX_P_4_5': 1,
    'ASC-OUTMATRIX_Y_4_5': 1}

###########################
## mich 
###########################

mich_input_power = 10
michdark_gain_acq = -2500#-333 # KI Jan-5-2016, was -400*0.67
michdark_gain = -2500 # KI Jan-5-2016, was -1000
michdark_locked_threshold = 120#50#130 / 3 #30

michbright_gain_acq = 2500# 300
michbright_gain = 2500 
michbright_locked_threshold = 30 # 62.5 from OM1 swap April 2015 ---EDH 2015-04-08
 

###########################
## prxy
###########################
prxy_gain = -3200 #-80 sheila increased sept 18, I'm not sure why I had to increase this to get 20 Hz ugf
prxy_asair_lock_threshold = 20 #200 / 4 # See note for michbright
prxy_asair_min_fringe = 1500 / 3 # See note for michbright
prxy_popairdc_oscillating_thresh = 6
prxy_REFL9_whiten_gain = 0 # REFL_A_RF9

###########################
## srxy
###########################
srxy_gain = -10000 #used to be -10000
srxy_input_power = 10
srxy_trig_on_thresh = 50
srxy_trig_off_thresh = 30

###########################
## arm IR
###########################
arm_IR_gain = dict()
arm_IR_gain['X'] = 0.05 # set back to nominal of 0.05, KI 18 Nov 2015. 0.15 was too high and gain peaking kept unlocking the loop.
# was 0.05 and doesn't lock, bumped up to 0.15, CV 18 Nov 2015 
#was 0.08 until August 18 2015 # was 0.12 as of Apr-16-2015 and bit too high
#arm_IR_gain['X'] = 0.2 #should be 0.2 to acquire lock 
arm_IR_gain['Y'] = 0.04
arm_IR_acquire_FMs = ['FM3', 'FM4', 'FM6'] #['FM8']
arm_IR_trig_on = 0.2
arm_IR_trig_off = .05
arm_IR_FM_trig_on = 0.25
arm_IR_FM_trig_off = 0.2
# Does anyone still use this whitening setting?
arm_ASAIR_whiten_gain = 6 # corresponding to a whitening gain of 18 dB

###########################
## RF45 modulation depth reduction
###########################
pdList = [
            'LSC-POP_A_RF45_I',
            'LSC-POP_A_RF45_Q',
            'ASC-AS_A_RF45_I_PIT',
            'ASC-AS_A_RF45_Q_PIT',
            'ASC-AS_A_RF45_I_YAW',
            'ASC-AS_A_RF45_Q_YAW',
            'ASC-AS_B_RF45_I_PIT',
            'ASC-AS_B_RF45_Q_PIT',
            'ASC-AS_B_RF45_I_YAW',
            'ASC-AS_B_RF45_Q_YAW',
            'ASC-REFL_A_RF45_I_PIT',
            'ASC-REFL_A_RF45_Q_PIT',
            'ASC-REFL_A_RF45_I_YAW',
            'ASC-REFL_A_RF45_Q_YAW',
            'ASC-REFL_B_RF45_I_PIT',
            'ASC-REFL_B_RF45_Q_PIT',
            'ASC-REFL_B_RF45_I_YAW',
            'ASC-REFL_B_RF45_Q_YAW',
            'ASC-AS_A_RF36_I_PIT',
            'ASC-AS_A_RF36_I_YAW',
            'ASC-AS_A_RF36_Q_PIT',
            'ASC-AS_A_RF36_Q_YAW',
            'ASC-AS_B_RF36_I_PIT',
            'ASC-AS_B_RF36_I_YAW',
            'ASC-AS_B_RF36_Q_PIT',
            'ASC-AS_B_RF36_Q_YAW',
            'ASC-POP_X_RF_I_PIT',
            'ASC-POP_X_RF_I_YAW',
            'ASC-POP_X_RF_Q_PIT',
            'ASC-POP_X_RF_Q_YAW',
            ]
pdSqList = [
            'LSC-POPAIR_B_RF90_I',
            'LSC-POPAIR_B_RF90_Q',
            'LSC-ASAIR_B_RF90_I',
            'LSC-ASAIR_B_RF90_Q',
            ]
pdListGain   = numpy.ones(len(pdList));
pdSqListGain = numpy.ones(len(pdSqList));
pdSqListGain[0]=4
pdSqListGain[1]=4


###########################
## RF9 modulation depth reduction
###########################
RF9modDepth = 16.8
pd9MHzlist = [
        'LSC-POP_A_RF9_I',
        'LSC-POP_A_RF9_Q',
        'LSC-REFL_A_RF9_I',
        'LSC-REFL_A_RF9_Q',
        'LSC-POPAIR_A_RF9_I',
        'LSC-POPAIR_A_RF9_Q',
        'LSC-REFLAIR_A_RF9_I',
        'LSC-REFLAIR_A_RF9_Q',
        'ASC-REFL_A_RF9_I1',
        'ASC-REFL_A_RF9_I2',
        'ASC-REFL_A_RF9_I3',
        'ASC-REFL_A_RF9_I4',
        'ASC-REFL_A_RF9_Q1',
        'ASC-REFL_A_RF9_Q2',
        'ASC-REFL_A_RF9_Q3',
        'ASC-REFL_A_RF9_Q4',
        'ASC-REFL_B_RF9_I1',
        'ASC-REFL_B_RF9_I2',
        'ASC-REFL_B_RF9_I3',
        'ASC-REFL_B_RF9_I4',
        'ASC-REFL_B_RF9_Q1',
        'ASC-REFL_B_RF9_Q2',
        'ASC-REFL_B_RF9_Q3',
        'ASC-REFL_B_RF9_Q4',
        'ASC-AS_A_RF36_I1',
        'ASC-AS_A_RF36_I2',
        'ASC-AS_A_RF36_I3',
        'ASC-AS_A_RF36_I4',
        'ASC-AS_A_RF36_Q1',
        'ASC-AS_A_RF36_Q2',
        'ASC-AS_A_RF36_Q3',
        'ASC-AS_A_RF36_Q4',
        'ASC-AS_B_RF36_I1',
        'ASC-AS_B_RF36_I2',
        'ASC-AS_B_RF36_I3',
        'ASC-AS_B_RF36_I4',
        'ASC-AS_B_RF36_Q1',
        'ASC-AS_B_RF36_Q2',
        'ASC-AS_B_RF36_Q3',
        'ASC-AS_B_RF36_Q4',
        ]

pd9MHzSqList = [
        'LSC-POPAIR_B_RF18_I',
        'LSC-POPAIR_B_RF18_Q',
        'LSC-ASAIR_B_RF18_I',
        'LSC-ASAIR_B_RF18_Q',
        ]

pd9MHzListGain   = numpy.ones(len(pdList));
pd9MHzSqListGain = numpy.ones(len(pdSqList));



###########################
## Feedforward
###########################
michFfGain = -15.9 
srclFfGain = -1.0 #was -1.1 June 7th 2017 SED changed#-0.55 for JFF  # -0.9 at 50W; -0.55 at 25W
srclFf2Gain = 0 #not needed with new filter -1.5 #was -1.3 June 7th 2017
##########################
## ESD biases to prefer
##########################
etmx_low_noise_bias_gain=-1  #-1 means -400 Volts I think
